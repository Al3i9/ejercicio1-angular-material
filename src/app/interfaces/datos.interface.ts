export interface FoodNodeI {
  name: string;
  children?: FoodNodeI[];
}
